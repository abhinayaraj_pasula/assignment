import React from "react";
import { Link } from "react-router-dom";

const Sidebar = () => {
  return (
    <div className="Sidebar">
      <Link to="/first" className="Link">
        First
      </Link>
      <Link to="/second" className="Link">
        Second
      </Link>
      <Link to="third" className="Link">
        Third
      </Link>
    </div>
  );
};

export default Sidebar;

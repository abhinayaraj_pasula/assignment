import React from "react";
import { Route, Redirect } from "react-router-dom";

import First from "./First";
import Second from "./Second";
import Third from "./Third";

const Main = () => {
  return (
    <div className="Main">
      <Route exact path="/" render={() => <Redirect to="/first" />} />
      <Route path="/first" component={First} />
      <Route path="/second" component={Second} />
      <Route path="/third" component={Third} />
    </div>
  );
};

export default Main;

import React from "react";
import {
  ResponsiveContainer,
  CartesianGrid,
  XAxis,
  YAxis,
  AreaChart,
  Tooltip,
  Area,
  BarChart,
  Legend,
  Bar
} from "recharts";

import { tableData, areaData, barData } from "./data";
class First extends React.Component {
  state = {
    isLoading: false,
    tableData: []
  };
  componentDidMount() {
    this.setState({ isLoading: true });
    setTimeout(() => {
      this.setState({ tableData, areaData, barData, isLoading: false });
    }, 400);
  }
  render() {
    const { isLoading, tableData, areaData } = this.state;
    if (isLoading) {
      return <div>Loading ... </div>;
    }
    return (
      <div className="TableAndChart">
        <div className="Table">
          <table>
            <thead>
              <tr>
                <th>S No.</th>
                <th>Name</th>
                <th>Standard</th>
                <th>Telugu</th>
                <th>Hindi</th>
                <th>English</th>
                <th>Maths</th>
                <th>Science</th>
                <th>Social</th>
                <th>Total</th>
                <th>Exam</th>
                <th>Month</th>
                <th>Year</th>
              </tr>
            </thead>
            <tbody>
              {tableData.map((element, index) => (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{element.name}</td>
                  <td>{element.standard}</td>
                  <td>{element.telugu}</td>
                  <td>{element.hindi}</td>
                  <td>{element.english}</td>
                  <td>{element.maths}</td>
                  <td>{element.science}</td>
                  <td>{element.social}</td>
                  <td>{element.total}</td>
                  <td>{element.exam}</td>
                  <td>{element.month}</td>
                  <td>{element.year}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <div className="Chart">
          <div className="Area">
            <ResponsiveContainer>
              <AreaChart
                data={areaData}
                margin={{ top: 0, right: 0, left: 0, bottom: 0 }}
              >
                <defs>
                  <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
                    <stop offset="5%" stopColor="#8884d8" stopOpacity={0.8} />
                    <stop offset="95%" stopColor="#8884d8" stopOpacity={0} />
                  </linearGradient>
                  <linearGradient id="colorPv" x1="0" y1="0" x2="0" y2="1">
                    <stop offset="5%" stopColor="#82ca9d" stopOpacity={0.8} />
                    <stop offset="95%" stopColor="#82ca9d" stopOpacity={0} />
                  </linearGradient>
                </defs>
                <XAxis dataKey="name" />
                <YAxis />
                <CartesianGrid strokeDasharray="3 3" />
                <Tooltip />
                <Area
                  type="monotone"
                  dataKey="marks"
                  stroke="#8884d8"
                  fillOpacity={1}
                  fill="url(#colorUv)"
                />
              </AreaChart>
            </ResponsiveContainer>
          </div>
          <div className="Bar">
            <ResponsiveContainer>
              <BarChart data={barData}>
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="name" />
                <YAxis />
                <Tooltip />
                <Legend />
                <Bar dataKey="marks" fill="#8884d8" />
              </BarChart>
            </ResponsiveContainer>
          </div>
        </div>
      </div>
    );
  }
}

export default First;

import React from "react";

export const tableData = [
  {
    name: "A",
    standard: 8,
    telugu: 80,
    hindi: 81,
    english: 87,
    maths: 78,
    science: 83,
    social: 90,
    total: 499,
    exam: "Annual",
    month: "April",
    year: 2019
  },
  {
    name: "B",
    standard: 8,
    telugu: 83,
    hindi: 86,
    english: 81,
    maths: 85,
    science: 89,
    social: 82,
    total: 506,
    exam: "Annual",
    month: "April",
    year: 2019
  },
  {
    name: "C",
    standard: 8,
    telugu: 77,
    hindi: 78,
    english: 79,
    maths: 71,
    science: 73,
    social: 75,
    total: 453,
    exam: "Annual",
    month: "April",
    year: 2019
  },
  {
    name: "D",
    standard: 8,
    telugu: 67,
    hindi: 70,
    english: 65,
    maths: 80,
    science: 81,
    social: 78,
    total: 441,
    exam: "Annual",
    month: "April",
    year: 2019
  },
  {
    name: "E",
    standard: 8,
    telugu: 63,
    hindi: 66,
    english: 62,
    maths: 70,
    science: 71,
    social: 72,
    total: 404,
    exam: "Annual",
    month: "April",
    year: 2019
  },
  {
    name: "F",
    standard: 8,
    telugu: 77,
    hindi: 78,
    english: 76,
    maths: 90,
    science: 95,
    social: 84,
    total: 500,
    exam: "Annual",
    month: "April",
    year: 2019
  },
  {
    name: "G",
    standard: 8,
    telugu: 86,
    hindi: 81,
    english: 83,
    maths: 87,
    science: 89,
    social: 95,
    total: 521,
    exam: "Annual",
    month: "April",
    year: 2019
  },
  {
    name: "H",
    standard: 8,
    telugu: 90,
    hindi: 90,
    english: 93,
    maths: 100,
    science: 99,
    social: 96,
    total: 568,
    exam: "Annual",
    month: "April",
    year: 2019
  },
  {
    name: "I",
    standard: 8,
    telugu: 80,
    hindi: 80,
    english: 81,
    maths: 89,
    science: 85,
    social: 82,
    total: 497,
    exam: "Annual",
    month: "April",
    year: 2019
  },
  {
    name: "J",
    standard: 8,
    telugu: 77,
    hindi: 72,
    english: 70,
    maths: 80,
    science: 79,
    social: 78,
    total: 456,
    exam: "Annual",
    month: "April",
    year: 2019
  },
  {
    name: "K",
    standard: 8,
    telugu: 75,
    hindi: 70,
    english: 68,
    maths: 78,
    science: 77,
    social: 76,
    total: 444,
    exam: "Annual",
    month: "April",
    year: 2019
  },
  {
    name: "L",
    standard: 8,
    telugu: 72,
    hindi: 67,
    english: 65,
    maths: 75,
    science: 74,
    social: 73,
    total: 426,
    exam: "Annual",
    month: "April",
    year: 2019
  }
];

export const areaData = [
  {
    name: "A",
    marks: 499
  },
  {
    name: "B",
    marks: 506
  },
  {
    name: "C",
    marks: 453
  },
  {
    name: "D",
    marks: 441
  },
  {
    name: "E",
    marks: 404
  },
  {
    name: "F",
    marks: 500
  },
  {
    name: "G",
    marks: 521
  },
  {
    name: "H",
    marks: 568
  },
  {
    name: "I",
    marks: 497
  },
  {
    name: "J",
    marks: 456
  },
  {
    name: "K",
    marks: 444
  },
  {
    name: "L",
    marks: 426
  }
];

export const barData = [
  {
    name: "A",
    marks: 499
  },
  {
    name: "B",
    marks: 506
  },
  {
    name: "C",
    marks: 453
  },
  {
    name: "D",
    marks: 441
  },
  {
    name: "E",
    marks: 404
  },
  {
    name: "F",
    marks: 500
  },
  {
    name: "G",
    marks: 521
  },
  {
    name: "H",
    marks: 568
  },
  {
    name: "I",
    marks: 497
  },
  {
    name: "J",
    marks: 456
  },
  {
    name: "K",
    marks: 444
  },
  {
    name: "L",
    marks: 426
  }
];
